﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Forms;

using ProvingGround.FreighterUtils;

namespace ProvingGround.FreightLauncher
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class formFreightLauncher : Window
    {

        #region private members

        private Executor _freight;
        private string _appDirectory;

        #endregion

        public formFreightLauncher()
        {
            
            InitializeComponent();

            // get the path of the executable
            string m_fullDirectory = System.IO.Path.GetDirectoryName(System.Reflection.Assembly.GetExecutingAssembly().GetName().CodeBase);
            _appDirectory = m_fullDirectory.Substring(6, m_fullDirectory.Length - 6);

            // create the executor
            _freight = new Executor(_appDirectory);

            // if the config file loads successfully, then do setup
            if (_freight.LoadConfig())
            {
                // widen scope
                DoSetup();
            }
            else
            {
                System.Windows.Forms.MessageBox.Show("Unable to find freight.xml in main directory.");
                this.Close();
            }

        }

        #region Private Members

        /// <summary>
        /// Main form setup
        /// </summary>
        private void DoSetup()
        {

            // updates form labels to reflect loading data
            label_rhDoc.Content = _freight.Attribute("RhinoFileName");
            label_ghDoc.Content = _freight.Attribute("GrasshopperFileName");

            // updates form logo to reflect user specified bitmap
            if (System.IO.File.Exists(_appDirectory + "\\Resources\\" + _freight.Attribute("LogoPath")))
            {
                Bitmap_Logo.Source = new BitmapImage(new Uri(_appDirectory + "\\Resources\\" + _freight.Attribute("LogoPath")));
            }

        }

        #endregion

        #region Form Operations

        /// <summary>
        /// Launches freight
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Button_Run_Click(object sender, RoutedEventArgs e)
        {

            _freight.LaunchFreight();
            this.Close();

        }

        /// <summary>
        /// Closes out of main form
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Button_Close_Click(object sender, RoutedEventArgs e)
        {

            this.Close();

        }

        #endregion
    }
}
