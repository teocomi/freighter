﻿using System;
using System.Runtime.InteropServices;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Rhino;
using Rhino.Commands;

using Grasshopper;
using Grasshopper.Kernel;

namespace ProvingGround.Freighter
{
    /// <summary>
    /// Guid Generator http://guidgenerator.com
    /// </summary>
    [Guid("6c627aa5-f09f-47be-9ee5-49dc3b169751")] //, CommandStyle(Style.Hidden)
    public class FreighterCmd : Command
    {
        public FreighterCmd()
        {
            // Rhino only creates one instance of each command class defined in a
            // plug-in, so it is safe to store a refence in a static property.
            Instance = this;
        }

        /// <summary>
        /// The only instance of this command.
        /// </summary>
        public static FreighterCmd Instance
        {
            get;
            private set;
        }

        /// <returns>
        /// The command name as it appears on the Rhino command line.
        /// </returns>
        public override string EnglishName
        {
            get { return "Freighter"; }
        }

        /// <summary>
        /// Called by Rhino when the user wants to run this command.
        /// </summary>
        protected override Result RunCommand(RhinoDoc doc, RunMode mode)
        {
            RhinoApp.WriteLine("{0} loaded.", FreighterPlugin.Instance.Name);

            Grasshopper.Kernel.GH_DocumentServer m_ghDocServer = Grasshopper.Instances.DocumentServer;
            if (m_ghDocServer.DocumentCount > 0)
            {
                Grasshopper.Kernel.GH_Document m_ghDoc = m_ghDocServer[0];
                GH_DocumentIO m_ghDocIO = new GH_DocumentIO(m_ghDoc);

                // if the current grasshopper document has not yet been saved, it must be first
                if (m_ghDoc.FilePath == null)
                {
                    RhinoApp.WriteLine("The target Grasshopper definition must first be saved.");
                    if (!m_ghDocIO.SaveAs())
                    {
                        RhinoApp.WriteLine("GH document not saved."); 
                        return Result.Failure;
                    }
                }
                // ensure that the grasshopper document currently open is properly saved
                else
                {
                    if (!m_ghDocIO.Save())
                    {
                        RhinoApp.WriteLine("GH document did not save properly."); 
                        return Result.Failure;
                    }
                    else
                    { 
                       RhinoApp.WriteLine("GH document saved to: " + m_ghDoc.FilePath, FreighterPlugin.Instance.Name);
                    }
                }
            }
            else
            {
                RhinoApp.WriteLine("The target Grasshopper plug-in must be loaded.", FreighterPlugin.Instance.Name);
                return Result.Failure;
            }

            try
            {
                Freighter.UI.formFreighter BuildFreight = new Freighter.UI.formFreighter(doc);
                BuildFreight.ShowDialog();
            }
            catch (Exception ex)
            {
                System.Windows.Forms.MessageBox.Show(ex.Message + ": " + ex.Source + "; " + ex.TargetSite.Name + ": " + ex.StackTrace);
                
                throw;
            }
            
            return Result.Success;

        }
    }
}
