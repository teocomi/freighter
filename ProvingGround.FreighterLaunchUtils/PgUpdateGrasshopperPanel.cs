﻿using System;
using System.Runtime.InteropServices;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Rhino;
using Rhino.Commands;

using Grasshopper.Kernel;

namespace ProvingGround.FreighterLaunchUtils
{
    /// <summary>
    /// Guid Generator http://guidgenerator.com
    /// </summary>
    [Guid("1e466ca5-fbd8-40bf-a370-82262f94f09f"), CommandStyle(Style.Hidden)]
    public class PgUpdateGrasshopperPanel : Command
    {
        public PgUpdateGrasshopperPanel()
        {

            // Rhino only creates one instance of each command class defined in a
            // plug-in, so it is safe to store a refence in a static property.
            Instance = this;

        }

        /// <summary>
        /// The only instance of this command.
        /// </summary>
        public static PgUpdateGrasshopperPanel Instance
        {

            get;
            private set;

        }

        /// <returns>
        /// The command name as it appears on the Rhino command line.
        /// </returns>
        public override string EnglishName
        {

            get { return "PgUpdateGrasshopperPanel"; }

        }

        /// <summary>
        /// Replaces the file lookup contents 
        /// </summary>
        protected override Result RunCommand(RhinoDoc doc, RunMode mode)
        {

            // Custom input GetString parameters can be fed to the command line
            // by stringing spaces together

            var m_goGuid = new Rhino.Input.Custom.GetString();
            m_goGuid.SetCommandPrompt("Instance Guid of Component");

            var m_goContent = new Rhino.Input.Custom.GetString();
            m_goContent.SetCommandPrompt("New File Content");

            m_goGuid.Get();
            m_goContent.Get();

            // Instance guid of component and content string
            Guid m_instanceGuid = Guid.Parse(m_goGuid.StringResult());
            
            // Content passed as comma separated string to be split out
            string[] m_contents = m_goContent.StringResult().
                Split(new string[] { "," }, StringSplitOptions.None);

            // get the current running instance of Grasshopper
            Grasshopper.Kernel.GH_DocumentServer m_ghDocServer = Grasshopper.Instances.DocumentServer;
            Grasshopper.Kernel.GH_Document m_ghDoc = m_ghDocServer[0];

            // get the component to update (either File Path or Panel)
            IGH_DocumentObject m_updateComponent = m_ghDoc.FindObject(m_instanceGuid, true);

            if (m_updateComponent != null)
            {

                //+ @"\Attachments\"

                // Get the app path for the Gh Document
                string m_ghDocDirectory = new System.IO.FileInfo(m_ghDoc.FilePath).DirectoryName;
                string m_attachmentRoot = System.IO.Directory.GetParent(m_ghDocDirectory).FullName + @"\Attachments\";

                if (m_updateComponent is Grasshopper.Kernel.Special.GH_Panel)
                {

                    // for GH_Panels
                    Grasshopper.Kernel.Special.GH_Panel m_updatePanel =
                               (Grasshopper.Kernel.Special.GH_Panel)m_updateComponent;

                    m_updatePanel.VolatileData.Clear();

                    // Reset the file path string to the current application location, and
                    // combine into multiline data where appropriate
                    int m_counter = 0;
                    string m_newText = "";

                    foreach (string m_content in m_contents)
                    {
                        if (m_counter > 0) m_newText += "\n";
                        m_counter += 1;
                        m_newText += m_attachmentRoot + m_content;
                    }

                    m_updatePanel.SetUserText(m_newText);

                }
                else if (m_updateComponent is Grasshopper.Kernel.Parameters.Param_FilePath)
                {

                    // for File Path Params
                    Grasshopper.Kernel.Parameters.Param_FilePath m_updateFilePath =
                        (Grasshopper.Kernel.Parameters.Param_FilePath)m_updateComponent;

                    var m_newText = new Grasshopper.Kernel.Data.GH_Structure<Grasshopper.Kernel.Types.GH_String>();

                    foreach (string m_content in m_contents)
                    {
                        m_newText.Append(new Grasshopper.Kernel.Types.GH_String(m_attachmentRoot + m_content));
                    }

                    m_updateFilePath.SetPersistentData(m_newText);

                }

            }

            return Result.Success;

        }
    }
}
