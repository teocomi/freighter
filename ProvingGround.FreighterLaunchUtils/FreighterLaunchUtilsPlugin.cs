﻿using System;
using System.Reflection;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Rhino;
using Rhino.PlugIns;

namespace ProvingGround.FreighterLaunchUtils
{
    public class FreighterLaunchUtilsPlugin : PlugIn
    {
        public FreighterLaunchUtilsPlugin()
        {
            Instance = this;
        }

        /// <summary>
        /// Gets the only instance of the WDIRhinoPlugin plug-in.
        /// </summary>
        public static FreighterLaunchUtilsPlugin Instance
        {
            get;
            private set;
        }

        /// <summary>
        /// Specifies when the plugin will load.
        /// </summary>
        public override Rhino.PlugIns.PlugInLoadTime LoadTime
        {
            get { return Rhino.PlugIns.PlugInLoadTime.AtStartup; }
        }

        /// <summary>
        /// Called by Rhino when loading this plug-in.
        /// </summary>
        protected override LoadReturnCode OnLoad(ref string errorMessage)
        {
            string app_name = Assembly.GetExecutingAssembly().GetName().Name;
            string app_version = Assembly.GetExecutingAssembly().GetName().Version.ToString();
            RhinoApp.WriteLine("{0} {1} loaded.", app_name, app_version);
            return LoadReturnCode.Success;
        }

        /// <summary>
        /// Override this function if you want to return a COM visible object to
        /// RhinoScript or an external application that is automating Rhino.
        /// </summary>
        public override object GetPlugInObject()
        {
            return null;
        }

    }
}
