﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Reflection;
using System.IO;
using System.Security;
using System.Security.AccessControl;

using Microsoft.Win32;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace ProvingGround.Freighter.Installer
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        // Target Paths
        private string _targetRevit2015AddinPath = System.IO.Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData), "Autodesk", "Revit", "Addins", "2015"); //Revit Addins Folder
        private string _targetRevit2016AddinPath = System.IO.Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData), "Autodesk", "Revit", "Addins", "2016"); //Revit Addins Folder
        private string _targetGrasshopperPath = System.IO.Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData), "Grasshopper", "Libraries"); //Grasshopper Addons Folder
        private string _targetDynamoPath = System.IO.Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData), "Dynamo", "0.8", "packages"); //Dynamo Addons Folder

        // Source path
        private string _sourcepath = System.IO.Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location);

        /// <summary>
        /// Constructor
        /// </summary>
        public MainWindow()
        {
            InitializeComponent();

            form_EULA m_eula = new form_EULA();
            m_eula.ShowDialog();

            if (m_eula.Acceptance == false)
            {
                this.Close();
            }

            Label_Version.Content = "v." + System.Reflection.Assembly.GetExecutingAssembly().GetName().Version.ToString();

        }

        /// <summary>
        /// Install new files
        /// </summary>
        private void InstallFiles()
        {

            try
            {
                if (Directory.Exists(@"C:\ProgramData\ProvingGround"))
                {
                    System.IO.Directory.Delete(@"C:\ProgramData\ProvingGround", true);
                }
            }
            catch
            {

            }

            try
            {
                if (!Directory.Exists(@"C:\Proving Ground\Rhino\Freighter")) Directory.CreateDirectory(@"C:\Proving Ground\Rhino\Freighter");

                System.IO.File.WriteAllBytes(@"C:\Proving Ground\Rhino\Freighter\ProvingGround.Freighter.rhp", ProvingGround.Freighter.Installer.Resources.ProvingGround_Freighter);
                System.IO.File.WriteAllBytes(@"C:\Proving Ground\Rhino\Freighter\ProvingGround.FreighterUtils.dll", ProvingGround.Freighter.Installer.Resources.ProvingGround_FreighterUtils);
                SetUpRhinoPlugIn(@"C:\Proving Ground\Rhino\Freighter\ProvingGround.Freighter.rhp");
                System.Windows.Forms.MessageBox.Show("Freighter has been successfully installed!");
            }
            catch { System.Windows.Forms.MessageBox.Show("Freighter has failed to install!"); }
            this.Close();
        }

        /// <summary>
        /// Create registry entries for Rhino plug-in for automatic loading
        /// </summary>
        /// <param name="PlugInPath">drive path to the plug-in</param>
        private bool SetUpRhinoPlugIn(string PlugInPath)
        {
            try
            {
                bool setup = false;

                //Registry Key Stuff
                //string m_rhinopath = "SOFTWARE\\McNeel\\Rhinoceros\\5.0x64";
                string m_rhkeypath = "SOFTWARE\\McNeel\\Rhinoceros\\5.0x64\\Plug-ins";
                string m_rhguid = "d3da725e-0af1-4fc1-a007-1b5bf631a5a0";
                string m_keyname = "Freighter";

                //user security to access registry
                RegistrySecurity m_userSecurity = new RegistrySecurity();
                RegistryAccessRule m_userRule = new RegistryAccessRule("Everyone", RegistryRights.FullControl, AccessControlType.Allow);
                m_userSecurity.AddAccessRule(m_userRule);

                /// current user path config
                try
                {
                    if (setup == false)
                    {
                        //create registry key
                        RegistryKey m_rhreg = Registry.CurrentUser;
                        m_rhreg = m_rhreg.OpenSubKey(m_rhkeypath, true);
                        if (m_rhreg != null)
                        {
                            // create plugin key
                            RegistryKey m_rhnewkey = m_rhreg.CreateSubKey(m_rhguid, RegistryKeyPermissionCheck.ReadWriteSubTree);
                            m_rhnewkey.SetValue("Name", m_keyname);
                            m_rhnewkey.SetValue("FileName", PlugInPath);
                            m_rhnewkey.Close();

                            setup = true;
                        }
                    }
                }
                catch { }

                /// try local machine config
                try
                {
                    if (setup == false)
                    {
                        //create registry key
                        RegistryKey m_rhreg = Registry.LocalMachine;
                        m_rhreg = m_rhreg.OpenSubKey(m_rhkeypath, true);
                        if (m_rhreg != null)
                        {
                            // create plugin key
                            RegistryKey m_rhnewkey = m_rhreg.CreateSubKey(m_rhguid, RegistryKeyPermissionCheck.ReadWriteSubTree);
                            m_rhnewkey.SetValue("Name", m_keyname);
                            m_rhnewkey.SetValue("FileName", PlugInPath);
                            m_rhnewkey.Close();

                            setup = true;
                        }
                    }
                }
                catch { }

                return setup;
            }
            catch { return false; }
        }


        /// <summary>
        /// Remove old, out of date files from previous versions
        /// </summary>
        private void CleanUpOldFiles()
        {
            //add fill paths here
            List<string> m_oldfiles = new List<string>();

            // delete files
            foreach (string f in m_oldfiles)
            {
                if (System.IO.File.Exists(f))
                {
                    System.IO.File.Delete(f);
                }
            }
        }

        /// <summary>
        /// Close Form
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Button_Close_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
        }

        /// <summary>
        /// Install Tools
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Button_Install_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                // install the files
                InstallFiles();
            }
            catch { }
        }

        /// <summary>
        /// Drag Window
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Window_MouseDown(object sender, MouseButtonEventArgs e)
        {
            if (e.ChangedButton == MouseButton.Left)
                this.DragMove();
        }
    }
}
